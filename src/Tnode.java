import java.util.*;
import java.util.regex.Pattern;

/** Tree with two pointers.
 * @since 1.8
 */
public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();

      if(name != null){
         b.append(name);
      }

      if(firstChild != null && firstChild.name != null){
         b.append("(");
         if(firstChild.nextSibling != null && firstChild.nextSibling.name != null){
            b.append(firstChild.nextSibling.toString());
         }
         b.append(",");
         b.append(firstChild.toString());
         b.append(")");
      }

      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      Tnode root;
      List<Tnode> stack = createNodeStack(pol);
      root = stack.get(stack.size() - 1);

      //exception for root, otherwise counting doesent work.
      if(stack.size() == 1 && isNumeric(root.name)){
         return root;
      }

      //stack counter is necessary to keep check that formed tree could be performed.
      int counter = -2;

      for (int i = stack.size() - 2; i >= 0; i--) {
         Tnode currentNode = stack.get(i);
         if(isNumeric(currentNode.name)){
            counter++;
         }else{
            counter--;
         }

         try {
            insertIntoSuitablePlace(currentNode, stack, i);
         } catch (Exception e) {
            throw new RuntimeException("Problem with " + pol + ". " + e.getMessage());
         }

      }
      if(counter > 0){
         throw new RuntimeException(pol + " has too many numbers");
      }
      if(counter < 0){
         throw new RuntimeException(pol + " hasn't got enough numbers");
      }
      return root;
   }

   private static void insertIntoSuitablePlace(Tnode currentNode, List<Tnode> stack, int i) {

      //Exception for root node, because it has only child, but no brother.
      if (i == stack.size() - 2 && stack.get(stack.size() - 1).firstChild == null) {
         stack.get(stack.size() - 1).firstChild = currentNode;
         return;
      }

      for (int j = i + 1; j < stack.size() - 1; j++) {
         Tnode previousMember = stack.get(j);

         //checking if previous node is open for a child.
         if (previousMember.firstChild == null && !(isNumeric(previousMember.name))) {
            previousMember.firstChild = currentNode;
            return;

         }//checking if previous node is open for a brother.
         else if (previousMember.nextSibling == null) {
            previousMember.nextSibling = currentNode;
            currentNode.nextSibling = new Tnode();
            return;
         }
      }
      //we did not find place to place current node.
      throw new RuntimeException("Nowhere to place " + currentNode.name);
   }


   private static List<Tnode> createNodeStack(String reversePN) {
      String[] stringArray = reversePN.split("\\s+");
      List<Tnode> stack = new ArrayList<>();
      for (String s : stringArray) {
         Tnode node = new Tnode();
         if (node.validName(s)) {
            node.name = s;
            stack.add(node);
         } else {
            throw new RuntimeException(s + " is not valid node value");
         }
      }
      return stack;
   }


   private boolean validName(String value) {
      if (isNumeric(value)) {
         return true;
      }
      String[] operators = new String[]{"+", "-", "/", "*"};
      for (String operator : operators) {
         if (value.equals(operator)) {
            return true;
         }
      }
      return false;
   }


   //https://www.baeldung.com/java-check-string-number
   private static boolean isNumeric(String strNum) {
      Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
      if (strNum == null) {
         return false;
      }
      return pattern.matcher(strNum).matches();
   }


   public static void main(String[] param) {
      String rpn = "2 1 - 4 * 6 3 / +";
      System.out.println("RPN: " + rpn);
      Tnode res = buildFromRPN(rpn);
      System.out.println("Tree: " + res);
      // TODO!!! Your tests here
   }
}

